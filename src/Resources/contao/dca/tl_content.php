<?php

/**
 * 361GRAD Element Images
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_images']       =
    '{type_legend},type,headline;' .
    '{switch_legend},dse_switch;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_imagessingle'] =
    '{type_legend},type,headline;' .
    '{switch_legend},dse_switch;' .
    '{imagesOne_legend},dse_imageOne,dse_imageAltOne,dse_imageSizeOne;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_imagesdouble'] =
    '{type_legend},type,headline;' .
    '{switch_legend},dse_switch;' .
    '{imagesOne_legend},dse_imageOne,dse_imageAltOne,dse_imageSizeOne;' .
    '{imagesTwo_legend},dse_imageTwo,dse_imageAltTwo,dse_imageSizeTwo;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['palettes']['__selector__'][] = 'dse_switch';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_switch'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['switch'],
    'inputType' => 'select',
    'options'   => ['single', 'double'],
    'reference' => &$GLOBALS['TL_LANG']['tl_content']['switch'],
    'eval'      => [
        'includeBlankOption' => true,
        'submitOnChange'     => true,
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageOne'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['singleSRC'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => 'jpg,jpeg,png,gif,svg',
        'tl_class'   => 'clr',
    ],
    'sql'       => 'binary(16) NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageAltOne'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['alt'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 clr',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageSizeOne'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageTwo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['singleSRC'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => 'jpg,jpeg,png,gif,svg',
        'tl_class'   => 'clr',
    ],
    'sql'       => 'binary(16) NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageAltTwo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['alt'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 clr',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageSizeTwo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];