<?php

/**
 * 361GRAD Element Images
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_images']   = ['Bilder', 'Bild groß oder 2 nebeneinander'];

$GLOBALS['TL_LANG']['tl_content']['dse_images']       = 'Bilder';
$GLOBALS['TL_LANG']['tl_content']['imagesOne_legend'] = 'Bild groß / Bild Links';
$GLOBALS['TL_LANG']['tl_content']['imagesTwo_legend'] = 'Bild Rechts';
$GLOBALS['TL_LANG']['tl_content']['switch_legend']    = 'Bilder Modus';
$GLOBALS['TL_LANG']['tl_content']['switch']           = [
    'Modus',
    'Hier wählen sie ob es ein großes Bild oder 2 nebeneinander sein sollen.',
    'single' => 'Einzel',
    'double' => 'Doppelt',
];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];