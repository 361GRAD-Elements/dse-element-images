<?php

/**
 * 361GRAD Element Images
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_images'] = ['Images', 'Big Image or 2 images side by side'];

$GLOBALS['TL_LANG']['tl_content']['dse_images']       = 'Images';
$GLOBALS['TL_LANG']['tl_content']['imagesOne_legend'] = 'Image big / Image left';
$GLOBALS['TL_LANG']['tl_content']['imagesTwo_legend'] = 'Image right';
$GLOBALS['TL_LANG']['tl_content']['switch_legend']    = 'Images mode';
$GLOBALS['TL_LANG']['tl_content']['switch']           = [
    'Mode',
    'Please choose the mode. One big single image or 2 images side by side.',
    'single' => 'Single',
    'double' => 'Double',
];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];